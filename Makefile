SCAD=jansen-linkage.scad support.scad
GIF=$(SCAD:.scad=.gif)
SCAD_GIF=jansen-linkage.scad
FRAMES=$(patsubst %, $(SCAD_GIF:.scad=)-%.png, $(shell LANG=C seq 0 0.01 1))
CAMERA=-30,-30,0,-30,-30,0,450
SIZE=1024,1024

all: $(SCAD:.scad=.svg) $(SCAD:.scad=.png)

gif: $(SCAD:.scad=.gif)

%.svg: %.scad
	openscad -D display=false -o $@ $<

%.png: %.scad
	openscad --render --imgsize $(SIZE) --camera $(CAMERA) -o $@ $<

$(GIF): $(FRAMES)
	convert $(FRAMES) $@

$(FRAMES): $(SCAD_GIF)
	openscad --render --imgsize $(SIZE) --camera $(CAMERA) -D '$$t=$(patsubst $(<:.scad=)-%.png,%,$@)' -o $@ $<

clean:
	rm *png *gcode *gif *svg

