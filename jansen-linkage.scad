width = 8;
hole  = 3.1;
hole2 = 4.2;
margin = width+5;
thickness = 5;
spacer = 1;
empty = true;
display = true;
//display = false;

// https://commons.wikimedia.org/wiki/File:Strandbeest_Leg_Proportions.svg
length_a = 38.0;
length_b = 41.5;
length_c = 39.3;
length_d = 40.1;
length_e = 55.8;
length_f = 39.4;
length_g = 36.7;
length_h = 65.7;
length_i = 49.0;
length_j = 50.0;
length_k = 61.9;
length_l =  7.8;
length_m = 15.0;

echo("Real length of the longest bar (in mm)", length_h + width);

function point_from_lengths (length1, length2, length3) =
    let (point1 = [0,0])
    let (point2 = [0,length1])
    let(angle  = acos((length3*length3 + length1*length1 - length2*length2)/(2*length1*length3)))
    [length3*cos(angle), length3*sin(angle)];

function distance(a, b) = sqrt( (a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]));

function rotation(angle) = [
     [cos(angle), -sin(angle)],
     [sin(angle),  cos(angle)]
    ];

function rotate(angle, point) = rotation(angle) * point;

function angle_2points(A, B) =
    let(B_A = B - A)
    atan2(B_A[1], B_A[0]);

function triangle_from_lengths (length1, length2, length3) =
    let (point1 = [0,0])
    let (point2 = [length1, 0])
    let (angle1  = acos((length3*length3 + length1*length1 - length2*length2)/(2*length1*length3)))
    let (angle2  = acos((length1*length1 + length2*length2 - length3*length3)/(2*length1*length2)))
    let (point3 = [length3*cos(angle1), length3*sin(angle1)])
    [angle1, angle2, point3];

module maybe_extrude (size)
{
    if (display) {
        linear_extrude (size) {
            children ();
        };
    }
    else {
        children ();
    };
}

module hole (diameter)
{
    radius = diameter / 2;
    fn = 100;
    fudge = 1/cos(180/fn);
    circle(r=radius*fudge,$fn=fn);
}

module bar (length, hole1=hole, hole2=hole)
{
    translate ([-width/2, -width/2, 0]) {
        maybe_extrude (thickness) {
            difference () {
                hull () {
                    translate([width/2, width/2]) {
                        circle(width/2);
                    }
                    translate([width/2+length, width/2]) {
                        circle(width/2);
                    }
                }
                translate ([width/2,width/2]) {
                    hole (hole1);
                }
                translate ([width/2+length,width/2]) {
                    hole (hole2);
                }
            }
        }
    }
}

module triangle (length1, length2, length3, hole1=hole, hole2=hole, hole3=hole)
{
    if (empty) {
        triangle_empty (length1, length2, length3, hole1=hole1, hole2=hole2, hole3=hole3);
    }
    else {
        triangle_full (length1, length2, length3, hole1=hole1, hole2=hole2, hole3=hole3);
    };
}

module triangle_full (length1, length2, length3, hole1=hole, hole2=hole, hole3=hole)
{
    point1 = [0,0];
    point2 = [length1, 0];

    angles = triangle_from_lengths(length1, length2, length3);
    angle1 = angles[0];
    angle2 = angles[1];

    point3 = [length3*cos(angle1), length3*sin(angle1)];

    maybe_extrude (thickness) {
        translate ([-width/2, -width/2]) {
            difference () {
                hull () {
                    translate([width/2, width/2]) {
                        circle(width/2);
                    }
                    translate(point2 + [width/2, width/2]) {
                        circle(width/2);
                    }
                    translate(point3 + [width/2, width/2]) {
                        circle(width/2);
                    }
                };
                translate (point1 + [width/2,width/2]) {
                    hole(hole1);
                };
                translate (point2 + [width/2,width/2]) {
                    hole(hole2);
                };
                translate (point3 + [width/2,width/2]) {
                    hole(hole3);
                };
            };
        };
    };
}

module triangle_empty (length1, length2, length3, hole1=hole, hole2=hole, hole3=hole)
{
    point1 = [0,0];
    point2 = [length1, 0];

    angles = triangle_from_lengths(length1, length2, length3);
    angle1 = angles[0];
    angle2 = angles[1];

    point3 = [length3*cos(angle1), length3*sin(angle1)];

    maybe_extrude (thickness) {
        translate ([-width/2, -width/2]) {
            difference () {
                union () {
                    hull () {
                        translate([width/2, width/2]) {
                            circle(width/2);
                        }
                        translate(point2 + [width/2, width/2]) {
                            circle(width/2);
                        }
                    };
                    hull () {
                        translate(point2 + [width/2, width/2]) {
                            circle(width/2);
                        }
                        translate(point3 + [width/2, width/2]) {
                            circle(width/2);
                        }
                    };
                    hull () {
                        translate([width/2, width/2]) {
                            circle(width/2);
                        }
                        translate(point3 + [width/2, width/2]) {
                            circle(width/2);
                        }
                    };
                };
                translate (point1 + [width/2,width/2]) {
                    hole(hole1);
                };
                translate (point2 + [width/2,width/2]) {
                    hole(hole2);
                };
                translate (point3 + [width/2,width/2]) {
                    hole(hole3);
                };
            };
        };
    };
}

module crankshaft () {
    bar(length_m);

//    maybe_extrude (thickness) {
//        radius = length_m + width;
//        difference () {
//            circle(radius);
//            hole(hole);
//            translate ([length_m, 0]) {
//                hole(hole);
//            };
//        }
////            echo(radius);
////            translate ([[100, 1000]]) {
////                hole(0.5*hole);
////            };
//        //};
//    };
};

module cut ()
{
    translate ([0, 0 * margin]) bar(length_k);
    translate ([0, 1 * margin]) bar(length_j);
    translate ([0, 2 * margin]) bar(length_f);
    translate ([0, 3 * margin]) bar(length_c, hole2=4);
    translate ([0, 4 * margin]) triangle(length_b, length_e, length_d, hole1=4);
    translate ([50, 8 * margin]) rotate (180, [0,0,1]) triangle(length_g, length_h, length_i, hole3=0);
    translate ([0, 9 * margin]) crankshaft();
    translate ([30, 9 * margin]) crankshaft();
    //translate ([32, 13 * margin]) rotate (90, [0,0,1]) support();
}

module rotate_center(a, v, center) {
    translate(center)
        rotate(a,v)
            translate(-center)
                children();   
}

module two_bars (length1, length2, length3) {
    point1 = [0,0];
    point2 = [length1, 0];

    angles = triangle_from_lengths(length1, length2, length3);
    angle1 = angles[0];
    angle2 = angles[1];

    point3 = [length3*cos(angle1), length3*sin(angle1)];
 
    rotate (angle1) {
        translate ([0, 0, - 2 * (thickness + spacer)]) {bar(length3);};
    }
    translate (point2) {
        rotate (180-angle2) {
            translate ([0, 0, - (thickness + spacer)]) {bar(length2);};
        }
    }
}

module one_bar (length1, length2, length3) {
    point1 = [0,0];
    point2 = [length1, 0];

    angles = triangle_from_lengths(length1, length2, length3);
    angle1 = angles[0];
    angle2 = angles[1];

    point3 = [length3*cos(angle1), length3*sin(angle1)];

    translate (point2) {
        rotate (180-angle2) {
            translate ([0, 0, -thickness - spacer]) {bar(length2);};
        }
    }
}

module support1 () {
    maybe_extrude (thickness) {
        difference () {
            hull () {
                circle(width/2);
                translate ([-length_a, -length_l]) {
                    circle(width/2);
                };
                translate ([0, -length_l]) {
                    circle(width/2);
                };
            };
            hole(hole);
            translate ([-length_a, -length_l]) {
                hole(hole);
            };
        };
    };
};

module support () {
    echo("Support: real width (in mm)", 2 * length_a + width);

    maybe_extrude (thickness) {
        difference () {
            union () {
                hull () {
                    translate ([-length_a, -length_l]) circle(width/2);
                    translate ([length_a, -length_l])  circle(width/2);
                };
                hull () {
                    translate ([-length_a, -length_l]) circle(width/2);
                    translate ([0, 30])                circle(width/2);
                }
                hull () {
                    translate ([-length_a, -length_l]) circle(width/2);
                    translate ([0, -30])               circle(width/2);
                }
                hull () {
                    translate ([length_a, -length_l]) circle(width/2);
                    translate ([0, 30])               circle(width/2);
                }
                hull () {
                    translate ([length_a, -length_l]) circle(width/2);
                    translate ([0, -30])              circle(width/2);
                }
               hull () {
                    translate ([0, 30])                circle(width/2);
                    translate ([0, -30])               circle(width/2);
                };
            };
          translate ([0, 0])                 hole(hole);
          translate ([-length_a, -length_l]) hole(hole2);
          translate ([length_a, -length_l])  hole(hole2);
          translate ([0, 30])                hole(hole2);
          translate ([0, -30])               hole(hole2);
        };
    };
};

module show (angle=-15)
{
    translate ([0, 0, thickness + spacer]) {
        support ();
    };
    
    rotate (angle, [0, 0, 1])  {
        crankshaft();
        translate ([0, 0, -5*(thickness + spacer)]) crankshaft();
    };
    
    leg(angle);
    rotate (180, [0,1,0]) {
        translate ([0, 0, 4*(thickness + spacer)]) leg(180-angle);
    }
}

module leg (angle) {
    // https://commons.wikimedia.org/wiki/File:Strandbeest_Leg_Proportions_Points_named.svg

    Z = [ 0, 0 ];
    Y = [ - length_a, - length_l ];
    X = [ length_m * cos(angle), length_m * sin(angle) ];

    length_YX = distance(Y, X);
    W = point_from_lengths (length_YX, length_j, length_b);
    alpha = angle_2points(Y, X);

    translate (X) {
        rotate (180+alpha, [0, 0, 1]) {
            two_bars(length_YX, length_c, length_k);
            triangle = triangle_from_lengths(length_YX, length_c, length_k);
            U = triangle[2];

            rotate (-180-alpha, [0, 0, 1]) {
                U = rotate(+180+alpha, U);

                translate (-X) {
                    U = U + X;

                    translate (Y) {
                        U = U - Y;

                        rotate (alpha, [0, 0, 1]) {
                            U = rotate(-alpha, U);

                            one_bar(length_YX, length_j, length_b);
                            triangle = triangle_from_lengths(length_YX, length_j, length_b);

                            rotate (triangle[0], [0, 0, 1]) {
                                U = rotate(-triangle[0], U);

                                triangle(length_b, length_e, length_d, hole1=4);
                                triangle = triangle_from_lengths(length_b, length_e, length_d);
                                V = triangle[2];

                                length_UV = distance(U, V);

                                translate (U) {
                                    rotate (triangle[0], [0, 0, 1]) {
                                        triangle = triangle_from_lengths(length_UV, length_c, length_d);
                                        rotate (-triangle[0], [0, 0, 1]) {
                                            one_bar(length_UV, length_f, length_g);
                                            triangle = triangle_from_lengths(length_UV, length_f, length_g);
                                            rotate (triangle[0], [0, 0, 1]) {
                                                triangle(length_g, length_h, length_i, hole3=0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if (display) {
    show (-360*$t);
}
else {
    cut();
}
